### Non Equilibrium Living Polymers Codes

This is a repo with codes to simulate the stress relaxation of polymers.

1. Unbreakable_Polymers.v1.c++ Simulates the relaxation via reptation of unbreakable polymers. Use as ./Unbreakable_Polymers.v1 L0 where L0 is the length of the polymers

2. Living_Polymers.v1.2.c++ Simulates the relaxation via reptation + breakage + fusion. Use as 
./Living_Polymers.v1.2 \xi where \xi is the adimensional number of breakage events per reptation time \xi=\tau_b/T_d (T_d is the reptation time)

3. Living_Polymers.v1.4.c++ Simulates the relaxation via reptation + breakage + fusion. Written to analyse the dependence on polymer length for fixed breakage **rate**. Use as ./Living_Polymers.v1.4 c1 A where c1 is the rate of breakage events per unit time and unit length c1=1/(\tau_b L0). The adimensional number of breakage events per reptation time is \xi = 1/(T_d c1 L0). A is a scale parameter for the length of the polymers which is A*L0 in this code. 

4. NonEq_Living_Polymers_breakage.v1.c++ Simulates the relaxation via reptation + breakage only. Use as ./NonEq_Living_Polymers_breakage.v1 \chi T_a where \chi=T_d/\tau_b is average number of breakage events per reptation time and T_a is the ageing time in units of reptation time. For simplicity the observation time (to compute the relaxation function) is equal to the reptation time. 

5. NonEq_Living_Polymers_fusion.v2.c++  Simulates the relaxation via reptation + fusion. Removes all arrays so that the relaxation can be followed for very long time. Use as ./NonEq_Living_Polymers_fusion.v2 \phi T_a where \phi is the number of fusion events per reptation time and T_a is the ageing time (in units of reptation time).
  
