/*
This code simulates the reptation model (eq.10) in Cates1987 with only fusion so the system is out of eq.
=> create a relaxation function per each "ageing block"
######
FOCUS ON L_0 DEPENDENCE!
SET c1 instead of xi -> easier for this purpose!
here c1=phi is the number of fusions per reptation time
######
REMOVE ALL ARRAYS BC RELAXATION TIME DIVERGES SO VERY LONG SIMULATIONS ARE NEEDED
Since there is no breakage when L>100 the relaxation is longer than the total simulation time so the sim is stopped and the longest relax time recorded.
#######
*/

#include<iostream>
#include<stdlib.h>
#include<cmath>
#include<sstream>
#include<fstream>
#include<string>
#include<iomanip>
#include<vector>
using namespace std;

/* CONSTANTS */
const int REPMAX=300;
const double dL=0.01; //
const double dt=0.01; //integration time
const double tau=1.0; //unit of time (100dt)
const double L0=1.0; //L0 sets the units of length (100dL)
const double D0=0.1; //monomer diffusion. In units of L0^2/dt
                        //if L0=1; set D0=(1/dt) to get L0^2 monomer diffusion every
                        //tau=(1/dt) steps. [D0]=0.01[L0^2/dt]=1[L0^2/tau]
const int bin=10;//for histogram. Time units
const int Tmaxmax=100000000; // this is bc it wants something not variable for arrays
const int Tmaxblock=100000; // this is bc it wants something not variable for arrays

/* ARRAYS */
double pos_left;
double pos_right;
double pos_left0; //start
double pos_right0; //start
int RelaxTime[REPMAX];
//int Survival[Tmaxmax];
//int SurvivalBlock[Tmaxblock][10]; //10 blocks

/* FUNCTIONS */
double Diff(double L);

/* MAIN */
int main(int argc, char* argv[]){
srand(time(NULL));
rand();

//cout << "Trep=" << Trep <<endl;

double phi=atof(argv[1]); //phi=Trep/tau_f number of fusions in one reptation time
double Trep=pow(L0,3.0)/(D0*3.14*3.14); //reptation time (units dt)
double Tbreak=Trep*1.0/phi;
int Tmax=int(100000*Trep); //total experiment time (units dt)
double c1=phi*1.0/Trep; //c1 in this case does not depend on length L0 and is 1/tau_f

//ageing block (in units of Tobs=Trep)
int block_number=atoi(argv[2]);

/*Initialise arrays*/
for(int i=0;i<REPMAX;i++)RelaxTime[i]=0;
//this is the time at which tube has relaxed; it is one number per replica

/* CREATE/OPEN FILES */
/*stringstream namepos;
namepos << "Position_phi"<<phi<<"_L"<<L0<<"_Block"<<block_number <<".dat";
ofstream writepos;
writepos.open(namepos.str().c_str());
*/
//
stringstream namefi;
namefi << "Survival_phi"<<phi<<"_L"<<L0<<"_Block"<<block_number <<".dat";
ofstream writeSu;
writeSu.open(namefi.str().c_str());
//
stringstream namefieta;
namefieta << "eta_phi"<<phi<<"_L"<<L0<<"_Block"<<block_number <<".dat";
ofstream writeETA;
writeETA.open(namefieta.str().c_str());
//

cout << "Start " <<endl;
/*Start loop over attempts*/
for(int i=0; i<REPMAX; i++){
cout << "============= " <<endl;
cout << "== NEW REP == " <<endl;
cout << "============= " <<endl;


/* Define initial condition with Length exponentially distributed ~exp(-l/Lc)
where L_c is L_0/n_c(tblock) = mean fragment length
*/

double Lmean;
int nfus;//=int(phi*block_number);
double prob=fmod(1.0*phi*block_number,1.0);
double rr=rand()*1.0/RAND_MAX;
if(rr<prob) nfus=ceil(phi*block_number);
else nfus=floor(phi*block_number);
Lmean=L0*(nfus+1);
cout << " prob " << prob << " & " << rr << " => " << Lmean << endl;
//cin.get();

//left end
pos_left0=-Lmean/2.*log(rand()*1.0/RAND_MAX); //sample from exponential distro
//right end
pos_right0=-Lmean/2.*log(rand()*1.0/RAND_MAX); //sampled from expoential ditro

/* Tube segment starts at 0 */
cout << 0 << " " << -pos_left0 <<" " << pos_right0<< " "<< i << endl;
//writepos << 0 << " " << -pos_left0 <<" " << pos_right0<< " "<< i << endl;

//cin.get();

double b1,b2,L;

/* =============== */
/*    START TIME   */
/* =============== */
/* Start time. The particle is at 0.*/
/* Pos_left is actually to negative value*/
for(int t=1;t<=Tmax/dt; t++){

if(t==1){
L=pos_right0+pos_left0;
b1=pos_left0;
b2=pos_right0;
}
else{
L=pos_right+pos_left;
b1=pos_left;
b2=pos_right;
}

/* At each timestep there is a chance of breakage or fusion */
/* BREAKAGE at rate c1 per unit time per unit length */
/*
for(int l=0;l<int(b1/dL);l++){
//cout << "attemp break @ " << l << endl;
double pbreak_left=rand()*1.0/RAND_MAX;
    if(pbreak_left<c1*dL*dt){
    //cout << "pbreak_left " << pbreak_left << " " << c1*dL*dt <<endl;
    cout << "LEFT BREAK " << b1<<" -> " << l*dL << endl; //cin.get();
    b1=l*dL;
    CountBreaks[i]++;
    break;
    }
}

for(int l=0;l<int(b2/dL);l++){
double pbreak_right=rand()*1.0/RAND_MAX;
    if(pbreak_right<c1*dL*dt){
    //cout << "pbreak_right " << pbreak_right << " " << c1*dL*dt <<endl;
    cout << "RIGHT BREAK " << b2<<" -> " << l*dL << endl; //cin.get();
    b2=l*dL;
    CountBreaks[i]++;
    break;
    }
}
*/

/* FUSION at rate c2 per unit time per unit length */
/* in reality it becomes c2*N(l)=c2*c1/c2*exp(-l/L0)=c1*exp(-l/L0) */
/* so propto the concentration of chains of length l */
//double l_left=-L0*log(rand()*1.0/RAND_MAX);
for(int l_left=1;l_left<1000;l_left++){
double pfus_left=rand()*1.0/RAND_MAX;
if(pfus_left<c1*dt*exp(-l_left/L0)){
    cout << "LEFT FUSION " << b1<<" -> " << b1+l_left << " @ "<< t << endl;
    b1=b1+l_left;
    break;
}
}
//double l_right=-L0*log(rand()*1.0/RAND_MAX);
for(int l_right=1;l_right<1000;l_right++){
double pfus_right=rand()*1.0/RAND_MAX;
if(pfus_right<c1*dt*exp(-l_right/L0)){
    cout << "RIGHT FUSION " << b2<<" -> " << b2+l_right << " @ "<< t << endl;
    b2=b2+l_right;
    break;
}
}


/* NEW LENGTH */
L=b2+b1;

/* UPDATE */
//pos_left[t]=b1;
//pos_right[t]=b2;

/* DIFFUSE THE TWO ENDS */
/* USE b1 and b2 otherwise missing the breaks */
/* CAREFUL! They are not independent otherwise the chain changes length! */
int dir=(rand()%2)*2-1; //+1 or -1
pos_left=b1-dir*sqrt(2*Diff(L)*dt);
pos_right=b2+dir*sqrt(2*Diff(L)*dt);

//cout << t*dt << " " << -pos_left << " " << pos_right << " "<< i << endl;
//writepos << t*dt << " " << -pos_left << " " << pos_right << " "<< i << endl;
//if(t%10000==0)cin.get();

/* Survival function */
if(pos_left>0 && pos_right>0){
/*survived*/
}

/* if one of them hit 0 break */
if(pos_left<0 || pos_right<0 || t==Tmax/dt){
RelaxTime[i]=t;
cout << "relaxed @ " << t <<endl;
break;
}

if(L>100){
RelaxTime[i]=Tmax/dt;
break;
}
//writel << i << "  " << t*dt << " " << L <<endl;
//InstLength[t]+=L;

}//close loop over time

//writel <<endl;
//writel <<endl;

//writepos << endl;
//writepos << endl;

}//close loop over attempts

cout << "finished attempts " << endl;

//cout << "lengths " << endl;
/*LENGTH*/
//for(int t=0;t<Tmax/dt;t++) writel << t*dt/Trep << " " << InstLength[t]*1.0/Survival[t]  << " " << InstLength[t] << " " << Survival[t] <<endl;

cout << "surv & visco " << endl;

/* SURVIVAL */
writeSu <<"#t/Trep P_survived(t) " << endl;
double eta=0;
for(int t=0;t<Tmax/dt;t++){
double mu_t=0;
    for(int i=0;i<REPMAX;i++){
    if(t<RelaxTime[i])mu_t++;
    }
    if(mu_t>0 && t%10==0)writeSu << t*dt*1.0/Trep << " " << mu_t*1.0/REPMAX<< endl;
    eta+=dt*mu_t*1.0/REPMAX;
}

writeETA << L0 << " " << phi << " " << block_number << " " << c1 << " " <<  eta <<endl;

writeSu <<endl;
writeSu <<endl;

for(int i=0;i<REPMAX;i++){
writeSu << i << " " << RelaxTime[i] <<endl;
}


/* VISCOSITY*/
//cout << "visco " << endl;
/* this is the integral of the survival function */
/*
double eta=0;
for(int i=0;i<Tmax/dt;i++) eta+=Survival[i]*1.0/REPMAX*dt;
cout << L0 << " " << phi << " " << c1 << " " <<  eta <<endl;
writeETA << "#L0 phi block c1 eta" <<endl;
writeETA << L0 << " " << phi << " " << block_number << " " << c1 << " " <<  eta <<endl;
*/

return 0;
}


/* curivlinear diffusion coeff and function of L */
double Diff(double L){
return D0/L; //L is in units of L0
}
