/*
This code simulates the reptation model (eq.10) in Cates1987 with only breakage so the system is out of eq.
=> create a relaxation function per each "ageing block"
######
FOCUS ON L_0 DEPENDENCE!
SET c1 instead of xi -> easier for this purpose!
######
1. The reptation of a polymer within a tube
2. breakage
=> How does the viscosity depend on the mean length?
*/

#include<iostream>
#include<stdlib.h>
#include<cmath>
#include<sstream>
#include<fstream>
#include<string>
#include<iomanip>
#include<vector>
using namespace std;

/* CONSTANTS */
const int REPMAX=800;
const double dL=0.01; //
const double dt=0.01; //integration time
const double tau=1.0; //unit of time (100dt)
const double L0=1; //L0 sets the units of length (100dL)
const double D0=0.01; //monomer diffusion. In units of L0^2/dt
                        //if L0=1; set D0=(1/dt) to get L0^2 monomer diffusion every
                        //tau=(1/dt) steps. [D0]=0.01[L0^2/dt]=1[L0^2/tau]
const int bin=10;//for histogram. Time units
const int Tmaxmax=10000000; // this is bc it wants something not variable for arrays
const int Tmaxblock=100000; // this is bc it wants something not variable for arrays

/* ARRAYS */
double pos_left[Tmaxmax];
double pos_right[Tmaxmax];
int RelaxTime[REPMAX];
int Histo[Tmaxmax];
int Survival[Tmaxmax];
int SurvivalBlock[Tmaxblock][10]; //10 blocks
int CountBreaks[REPMAX];
int CountFusions[REPMAX];
double InstLength[Tmaxmax];

/* FUNCTIONS */
double Diff(double L);

/* MAIN */
int main(int argc, char* argv[]){
srand(time(NULL));
rand();
//cout << "Trep=" << Trep <<endl;

double chi=atof(argv[1]);
double Trep=pow(L0,3.0)/(D0*3.14*3.14); //reptation time (units dt)
double Tbreak=Trep*1.0/chi;
int Tmax=int(100*Trep); //total experiment time (units dt) = observation time
double c1=chi*1.0/Trep*1.0/L0;

//ageing block (in units of Tobs=Trep)
int block_number=atoi(argv[2]);

/*Initialise arrays*/
for(int t=0;t<Tmaxmax;t++) pos_left[t]=pos_right[t]=Survival[t]=0;
for(int t=0;t<Tmaxblock;t++)for(int i=0;i<10;i++) SurvivalBlock[t][i]=0;
for(int i=0;i<REPMAX;i++)RelaxTime[i]=Tmax;

/* CREATE/OPEN FILES */
stringstream namepos;
namepos << "Position_chi"<<chi<<"_L"<<L0<<"_Block"<<block_number <<".dat";
ofstream writepos;
writepos.open(namepos.str().c_str());
//
stringstream namefi;
namefi << "Survival_chi"<<chi<<"_L"<<L0<<"_Block"<<block_number <<".dat";
ofstream writeSu;
writeSu.open(namefi.str().c_str());
//
stringstream namefieta;
namefieta << "eta_chi"<<chi<<"_L"<<L0<<"_Block"<<block_number <<".dat";
ofstream writeETA;
writeETA.open(namefieta.str().c_str());
//
stringstream namefitopo;
namefitopo << "topo_chi"<<chi<<"_L"<<L0<<"_Block"<<block_number <<".dat";
ofstream writetopo;
writetopo.open(namefitopo.str().c_str());
//
stringstream namefil;
namefil << "length_chi"<<chi<<"_L"<<L0<<"_Block"<<block_number <<".dat";
ofstream writel;
writel.open(namefil.str().c_str());
//

cout << "Start " <<endl;
/*Start loop over attempts*/
for(int i=0; i<REPMAX; i++){
cout << "============= " <<endl;
cout << "== NEW REP == " <<endl;
cout << "============= " <<endl;


/* Define initial condition with Length exponentially distributed ~exp(-l/Lc)
where L_c is L_0/n_c(tblock) = mean fragment length
*/

double Lcut;
int ncut; //=int(chi*block_number);
double prob=fmod(1.0*chi*block_number,1.0);
double rr=rand()*1.0/RAND_MAX;
if(rr<prob) ncut=ceil(chi*block_number);
else ncut=floor(chi*block_number);
Lcut=L0*1.0/(ncut+1);

//left end
pos_left[0]=-(Lcut/2.)*log(rand()*1.0/RAND_MAX); //sample from exponential distro
//right end
pos_right[0]=-(Lcut/2.)*log(rand()*1.0/RAND_MAX); //sampled from expoential ditro

/* Tube segment starts at 0 */
cout << 0 << " " << -pos_left[0] <<" " << pos_right[0]<< " "<< i << endl;
writepos << 0 << " " << -pos_left[0] <<" " << pos_right[0]<< " "<< i << endl;

//cin.get();

double b1,b2,L;

/* =============== */
/*    START TIME   */
/* =============== */
/* Start time. The particle is at 0.*/
/* Pos_left is actually to negative value*/
for(int t=1;t<Tmax/dt; t++){

L=pos_right[t-1]+pos_left[t-1];
b1=pos_left[t-1];
b2=pos_right[t-1];

/* At each timestep there is a chance of breakage or fusion */
/* BREAKAGE at rate c1 per unit time per unit length */
for(int l=0;l<int(b1/dL);l++){
//cout << "attemp break @ " << l << endl;
double pbreak_left=rand()*1.0/RAND_MAX;
    if(pbreak_left<c1*dL*dt){
    //cout << "pbreak_left " << pbreak_left << " " << c1*dL*dt <<endl;
    cout << "LEFT BREAK " << b1<<" -> " << l*dL << endl; //cin.get();
    b1=l*dL;
    CountBreaks[i]++;
    break;
    }
}

for(int l=0;l<int(b2/dL);l++){
double pbreak_right=rand()*1.0/RAND_MAX;
    if(pbreak_right<c1*dL*dt){
    //cout << "pbreak_right " << pbreak_right << " " << c1*dL*dt <<endl;
    cout << "RIGHT BREAK " << b2<<" -> " << l*dL << endl; //cin.get();
    b2=l*dL;
    CountBreaks[i]++;
    break;
    }
}


/* NEW LENGTH */
L=(b2+b1);

/* UPDATE */
//pos_left[t]=b1;
//pos_right[t]=b2;

/* DIFFUSE THE TWO ENDS */
/* USE b1 and b2 otherwise missing the breaks */
/* CAREFUL! They are not independent otherwise the chain changes length! */
int dir=(rand()%2)*2-1; //+1 or -1
pos_left[t]=b1-dir*sqrt(2*Diff(L)*dt);
pos_right[t]=b2+dir*sqrt(2*Diff(L)*dt);

cout << t*dt << " " << -pos_left[t] << " " << pos_right[t] << " "<< i << endl;
writepos << t*dt << " " << -pos_left[t] << " " << pos_right[t] << " "<< i << endl;
//if(t%10000==0)cin.get();

/* Survival function */
if(pos_left[t]>0 && pos_right[t]>0){
Survival[t]++;
}

/* if one of them hit 0 break */
if(pos_left[t]<0 || pos_right[t]<0){
RelaxTime[i]=t;
break;
}

//writel << i << "  " << t*dt << " " << L <<endl;
InstLength[t]+=L;

}//close loop over time

//writel <<endl;
//writel <<endl;

writepos << endl;
writepos << endl;

}//close loop over attempts

cout << "finished attempts " << endl;
cout << "lengths " << endl;

/*LENGTH*/
for(int t=0;t<Tmax/dt;t++) writel << t*dt/Trep << " " << InstLength[t]*1.0/Survival[t]  << " " << InstLength[t] << " " << Survival[t] <<endl;

cout << "surv " << endl;

/* SURVIVAL */
writeSu <<"#t/Trep P_survived(t) " << endl;
for(int t=0;t<Tmax/dt;t++){
if(Survival[t]>0) writeSu << t*dt*1.0/Trep << " " << Survival[t]*1.0/REPMAX<< endl;
}


/* VISCOSITY*/
cout << "visco " << endl;
/* this is the integral of the survival function */
double eta=0;
for(int i=0;i<Tmax/dt;i++) eta+=Survival[i]*1.0/REPMAX*dt;
cout << L0 << " " << chi << " " << c1 << " " <<  eta <<endl;
writeETA << "#L0 chi block c1 eta" <<endl;
writeETA << L0 << " " << chi << " " << block_number << " " << c1 << " " <<  eta <<endl;


double meanb=0;
double meanfus=0;
for(int i=0;i<REPMAX;i++){
writetopo << i << " " << CountBreaks[i] << " " << CountFusions[i] << " " << RelaxTime[i] <<endl;
meanb+=CountBreaks[i]*1.0/RelaxTime[i];
meanfus+=CountFusions[i]*1.0/RelaxTime[i];
}
writetopo<<endl;
writetopo<<endl;
writetopo << "Chi c_1 <breaks/time> <fusion/time> " <<endl;
writetopo << chi << " " << c1 <<" " << meanb/REPMAX << " " << meanfus/REPMAX <<endl;

return 0;
}


/* curivlinear diffusion coeff and function of L */
double Diff(double L){
return D0/L; //L is in units of L0
}
